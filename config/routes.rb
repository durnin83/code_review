Rails.application.routes.draw do
  resources :vocabulary_lists # do
  #   post 'import', on: :member
  # end

  root to: 'beta/beta#index'
  namespace :beta do
    resources :users, only: [:new, :create]
    get 'thank_you', to: 'beta#thank_you'
  end

  #get 'products/:id', to: 'products#show', :as => :products
  #devise_for :users
  #resources :users
end
