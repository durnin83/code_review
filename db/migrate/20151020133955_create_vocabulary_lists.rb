class CreateVocabularyLists < ActiveRecord::Migration
  def change
    create_table :vocabulary_lists do |t|
      t.string :name
      t.string :group
      t.text :description

      t.timestamps null: false
    end
  end
end
