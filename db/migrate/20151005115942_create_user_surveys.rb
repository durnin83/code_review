class CreateUserSurveys < ActiveRecord::Migration
  def change
    create_table :user_surveys do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.string :code
      t.text :answer

      t.timestamps null: false
    end
  end
end
