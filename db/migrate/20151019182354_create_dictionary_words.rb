class CreateDictionaryWords < ActiveRecord::Migration
  def change
    create_table :dictionary_words do |t|
      t.string :word
      t.string :part_of_speech
      t.string :translation
      t.text :definition
      t.text :notes

      t.timestamps null: false
    end
  end
end
