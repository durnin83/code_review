class CreateVocabularyListWords < ActiveRecord::Migration
  def change
    create_table :vocabulary_list_words do |t|
      t.belongs_to :vocabulary_list, index: true, foreign_key: true
      t.string :word
      t.string :part_of_speech
      t.string :translation
      t.text :definition
      t.text :notes

      t.timestamps null: false
    end
  end
end
