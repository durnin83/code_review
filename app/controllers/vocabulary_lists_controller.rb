class VocabularyListsController < ApplicationController
  before_action :set_vocabulary_list, only: [:show, :edit, :update, :destroy]

  # GET /vocabulary_lists
  # GET /vocabulary_lists.json
  def index
    @vocabulary_lists = VocabularyList.all
  end

  # GET /vocabulary_lists/1
  # GET /vocabulary_lists/1.json
  def show
  end

  # GET /vocabulary_lists/new
  def new
    @vocabulary_list = VocabularyList.new
  end

  # GET /vocabulary_lists/1/edit
  def edit
  end

  # POST /vocabulary_lists
  # POST /vocabulary_lists.json
  def create
    puts "vocabulary_list_params=#{vocabulary_list_params.inspect}"
    @vocabulary_list = VocabularyList.new(vocabulary_list_params)

    respond_to do |format|
      if @vocabulary_list.save
        format.html { redirect_to vocabulary_lists_path, notice: 'Vocabulary list was successfully created.' }
        format.json { render :show, status: :created, location: @vocabulary_list }
      else
        format.html { render :new }
        format.json { render json: @vocabulary_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vocabulary_lists/1
  # PATCH/PUT /vocabulary_lists/1.json
  def update
    respond_to do |format|
      if @vocabulary_list.update(vocabulary_list_params)
        format.html { redirect_to @vocabulary_list, notice: 'Vocabulary list was successfully updated.' }
        format.json { render :show, status: :ok, location: @vocabulary_list }
      else
        format.html { render :edit }
        format.json { render json: @vocabulary_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vocabulary_lists/1
  # DELETE /vocabulary_lists/1.json
  def destroy
    @vocabulary_list.destroy
    respond_to do |format|
      format.html { redirect_to vocabulary_lists_url, notice: 'Vocabulary list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vocabulary_list
      @vocabulary_list = VocabularyList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vocabulary_list_params
      params.require(:vocabulary_list).permit(:name, :group, :description, :word_list_file)
    end
end
