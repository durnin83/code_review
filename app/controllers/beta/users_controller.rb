class Beta::UsersController < ApplicationController
  # GET /beta/users/new
  def new
    @user = User.new
    UserSurvey::SURVEY_QUESTIONS.keys.each { |kode| @user.user_surveys.build(code: kode) }
  end

  # POST /beta/users
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to beta_thank_you_path, notice: 'User successfully registered for beta.' }
      else
        #flash[:notice] = 'Some of the fields have errors.'
        format.html { render action: 'new' }
      end
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :phone, user_surveys_attributes: [ :code, :answer ])
  end
end