module UserSurveyHelper
  def user_survey_field(form)
    label_text = UserSurvey::SURVEY_QUESTIONS[form.object.code]
    case UserSurvey::SURVEY_QUESTIONS_TYPES[form.object.code]
      when 'text'
        form.text_field :answer, label: label_text, placeholder: 'Your answer here', wrapper: {class: 'text-left'}
      when 'select'
        select_options = UserSurvey::SURVEY_QUESTIONS_OPTIONS[form.object.code].map { |value| [ value, value ] }
        form.select :answer, select_options, { label: label_text, prompt: '<Please select an option>' }, wrapper_class: 'text-left'
    end
  end
end
