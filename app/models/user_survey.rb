class UserSurvey < ActiveRecord::Base
  belongs_to :user

  SURVEY_QUESTIONS = {
      'about'           => 'How did you find about Smart Gongbu?',
      'intention'       => 'How do you intend to use Smart Gongbu?',
      'more_information'=> 'Is it ok if we contact you to give you more information about Smart Gongbu?',
      'feedback'        => 'Is it ok if we contact you for feedback on Smart Gongbu?'
  }

  # could be text, select, check
  SURVEY_QUESTIONS_TYPES = {
      'about'           => 'select',
      'intention'       => 'select',
      'more_information'=> 'select',
      'feedback'        => 'select'
  }

  # this is in case questions are of type select
  SURVEY_QUESTIONS_OPTIONS = {
      'about'           => ['Referral by a friend', 'Referral by a teacher', 'Social Media', 'Other'],
      'intention'       => ['To teach', 'To learn'],
      'more_information'=> ['Yes', 'No'],
      'feedback'        => ['Yes', 'No']
  }

  validates :user, presence: true
  validates :code, inclusion: { in: SURVEY_QUESTIONS.keys }, presence: true
  validates :answer, presence: true
  validate :answer_inclusion
  validate :duplicated_answer_for_user

  private

    def duplicated_answer_for_user
      if UserSurvey.where(self.attributes.slice(:user, :code)).count > 1
        errors[:base] << "Duplicated answer for user"
      end
    end

    def answer_inclusion
      error = false
      case SURVEY_QUESTIONS_TYPES[code]
        when 'select'
          error = !(SURVEY_QUESTIONS_OPTIONS[code].include?(answer))
        when 'check'
          error = !(['Y', 'N'].include?(answer))
      end
      errors[:answer] << "is not included in the list" if error
    end

end
