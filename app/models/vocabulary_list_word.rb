class VocabularyListWord < ActiveRecord::Base
  belongs_to :vocabulary_list

  validates :vocabulary_list, :word, :part_of_speech, :translation, :definition, presence: true
  validates :part_of_speech, inclusion: { in: DictionaryWord::PART_OF_SPEECH_VALUES }
  validates :word, length: { maximum: 1,
                             tokenizer: lambda { |str| str.split(/\s+/) },
                             too_long: "must be a single word" }
  validates :word, format: { with: /\A[a-zA-Z]+\z/,
                             message: "only allows letters" }
  validate :duplicated_vocabulary_list_word

  private

  def duplicated_vocabulary_list_word
    if VocabularyListWord.where(self.attributes.slice('vocabulary_list_id', 'word', 'part_of_speech', 'translation', 'definition', 'notes')).count > 1
      errors[:base] << "Word '#{word}' already in the vocabulary list with the same definition"
    end
  end

end
