class VocabularyList < ActiveRecord::Base
  extend SpreadsheetHelper

  attr_accessor :word_list_file

  has_many :vocabulary_list_words, :inverse_of => :vocabulary_list, :dependent => :destroy
  accepts_nested_attributes_for :vocabulary_list_words, allow_destroy: true

  validates :name, :group, :description, presence: true
  validates_associated :vocabulary_list_words

  before_validation :import_word_list_from_file, if: "word_list_file.present?"

  private

  def import_word_list_from_file
    spreadsheet = VocabularyList.open_spreadsheet word_list_file
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      vocabulary_list_word_atts = row.to_hash.slice(*VocabularyListWord.new.attributes.keys)
      if self.vocabulary_list_words.find_by(vocabulary_list_word_atts).blank?
        self.vocabulary_list_words.build(vocabulary_list_word_atts)
      end
    end
  end

end
