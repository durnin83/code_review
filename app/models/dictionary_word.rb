class DictionaryWord < ActiveRecord::Base
  PART_OF_SPEECH_VALUES = %w(noun verb adjective adverb pronoun preposition conjunction interjection numeral article determiner)
  validates :word, :part_of_speech, :translation, :definition, presence: true
  validates :part_of_speech, inclusion: { in: PART_OF_SPEECH_VALUES }
  validates :word, length: { maximum: 1,
                             tokenizer: lambda { |str| str.split(/\s+/) },
                             too_long: "must be a single word" }
  validates :word, format: { with: /\A[a-zA-Z]+\z/,
                             message: "only allows letters" }
  validate :duplicated_dictionary_word

  private

    def duplicated_dictionary_word
      if DictionaryWord.where(self.attributes.slice('word', 'part_of_speech', 'translation', 'definition', 'notes')).count > 1
        errors[:base] << "Word '#{word}' already in the dictionary with the same definition"
      end
    end
end
