class User < ActiveRecord::Base
  has_many :user_surveys, :inverse_of => :user, :dependent => :destroy
  accepts_nested_attributes_for :user_surveys, allow_destroy: true

  enum role: { beta: 0, user: 1, admin: 2 }

  with_options unless: :beta? do |beta_user|
    beta_user.validates :name, presence: true
    beta_user.validates :name, uniqueness: { case_sensitive: false }
  end
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, email: true, uniqueness: true
  validates :phone, phone: true, presence: true

  after_initialize :set_default_role, :if => :new_record?
  after_create :sign_up_for_mailing_list

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  def sign_up_for_mailing_list
    MailingListSignupJob.perform_later(self)
  end

  def subscribe
    mailchimp = Gibbon::Request.new(api_key: Rails.application.secrets.mailchimp_api_key)
    list_id = Rails.application.secrets.mailchimp_list_id
    result = mailchimp.lists(list_id).members.create(
      body: {
        email_address: self.email,
        status: 'subscribed'
    })
    Rails.logger.info("Subscribed #{self.email} to MailChimp") if result
  end

  private

  def set_default_role
    self.role ||= :beta
  end
end
