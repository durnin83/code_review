# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$('#video_link').click ->
  src = 'https://www.youtube.com/embed/NTrW4DsIwp4?rel=0&amp;showinfo=0&autoplay=1'
  $('#video_modal').modal 'show'
  $('#video_modal iframe').attr 'src', src
  return
$('#video_modal button').click ->
  $('#video_modal iframe').removeAttr 'src'
  return