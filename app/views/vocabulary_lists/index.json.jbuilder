json.array!(@vocabulary_lists) do |vocabulary_list|
  json.extract! vocabulary_list, :id, :name, :group, :description
  json.url vocabulary_list_url(vocabulary_list, format: :json)
end
