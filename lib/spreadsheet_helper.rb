module SpreadsheetHelper

  class UnknownSpreadsheetExtension < RuntimeError
  end

  def get_file_type(file)
    File.extname(file.original_filename).gsub('.','')
  end

  def open_spreadsheet(file)
    extension = get_file_type(file)
    if extension.in?(%w(csv xls xlsx))
      Roo::Spreadsheet.open(file.path, extension: extension)
    else
      raise UnknownSpreadsheetExtension "Unknown file type: #{file.original_filename}"
    end
  end

end