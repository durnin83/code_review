describe VocabularyListWord do
  before(:each) do
    @vocabulary_list_word = FactoryGirl.create(:vocabulary_list_word)
  end

  subject { @vocabulary_list_word }

  it { should respond_to(:vocabulary_list) }
  it { should respond_to(:word) }
  it { should respond_to(:part_of_speech) }
  it { should respond_to(:translation) }
  it { should respond_to(:definition) }
  it { should respond_to(:notes) }

  it { should belong_to(:vocabulary_list) }
  it { should validate_presence_of(:vocabulary_list) }
  it { should validate_presence_of(:word) }
  it { should validate_presence_of(:part_of_speech) }
  it { should validate_presence_of(:translation) }
  it { should validate_presence_of(:definition) }
  it do
    should validate_inclusion_of(:part_of_speech).in_array(DictionaryWord::PART_OF_SPEECH_VALUES)
  end
  it { should allow_value('this', 'are', 'All', 'WORDS').for(:word) }
  it { should_not allow_value('th1s', 'ar3nt', 'V@lid', 'W0rds').for(:word) }

  context "when new vocabulary list word has the same definition as an exisiting word" do
    before(:example) do
      @vocabulary_list_word2 = FactoryGirl.create(:vocabulary_list_word, vocabulary_list: @vocabulary_list_word.vocabulary_list,
                                                  word: @vocabulary_list_word.word,
                                                  part_of_speech: @vocabulary_list_word.part_of_speech,
                                                  translation: @vocabulary_list_word.translation,
                                                  definition: @vocabulary_list_word.definition,
                                                  notes: @vocabulary_list_word.notes)
    end

    it "word is not valid" do
      expect(@vocabulary_list_word2).not_to be_valid
    end

  end

  context "when new vocabulary list word is composed of more than one word" do
    before(:example) do
      @vocabulary_list_word2 = FactoryGirl.build(:two_words_vocabulary_list_word)
    end

    it "word is not valid" do
      expect(@vocabulary_list_word2).not_to be_valid
    end
  end
end
