describe User do
  before(:each) do
    @user = FactoryGirl.build(:user, :beta)
    @user.phone = '02 312 3456' # valid s.korean phone
  end

  subject { @user }

  context "when beta user" do
    before { allow(subject).to receive(:beta?).and_return(true) }
    it { should_not validate_presence_of(:name) }
    it { should_not validate_uniqueness_of(:name).case_insensitive }
  end

  context "when not beta user" do
    before { allow(subject).to receive(:beta?).and_return(false) }
    it { should validate_presence_of(:name) }
    #it { should validate_uniqueness_of(:name).case_insensitive }
  end

  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_presence_of(:phone) }

  it { should define_enum_for(:role) }
  it { should have_many(:user_surveys) }
  it do
    should accept_nested_attributes_for(:user_surveys).allow_destroy(true)
  end
  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:phone) }

  context "when email format is invalid" do
    it "user is not valid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
      end
    end
  end

  context "when email format is valid" do
    it "user is valid" do
      addresses = %w[user@gmail.com pepito_cuervo@hotmail.com cucu@msn.com]
      addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user).to be_valid
      end
    end
  end

  context "when phone format is invalid" do
    it "user is not valid" do
      phones = ['59a3125100', '6008974bd', 'aaa1234', '0*/1234', '4#12346755']
      phones.each do |invalid_phone|
        @user.phone = invalid_phone
        expect(@user).not_to be_valid
      end
    end
  end

  context "when phone format is valid" do
    it "user is valid" do
      phones = %w(+41446681800)
      phones.each do |valid_phone|
        @user.phone = valid_phone
        expect(@user).to be_valid
      end
    end
  end

end
