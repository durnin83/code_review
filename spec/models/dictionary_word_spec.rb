describe DictionaryWord do
  before(:each) do
    @dictionary_word = FactoryGirl.create(:dictionary_word)
  end

  subject { @dictionary_word }

  it { should respond_to(:word) }
  it { should respond_to(:part_of_speech) }
  it { should respond_to(:translation) }
  it { should respond_to(:definition) }
  it { should respond_to(:notes) }

  it { should validate_presence_of(:word) }
  it { should validate_presence_of(:part_of_speech) }
  it { should validate_presence_of(:translation) }
  it { should validate_presence_of(:definition) }
  it do
    should validate_inclusion_of(:part_of_speech).in_array(DictionaryWord::PART_OF_SPEECH_VALUES)
  end
  it { should allow_value('this', 'are', 'All', 'WORDS').for(:word) }
  it { should_not allow_value('th1s', 'ar3nt', 'V@lid', 'W0rds').for(:word) }

  context "when new dictionary word has the same definition as an exisiting word" do
    before(:example) do
      @dictionary_word2 = FactoryGirl.create(:dictionary_word, word: @dictionary_word.word,
                                             part_of_speech: @dictionary_word.part_of_speech,
                                             translation: @dictionary_word.translation,
                                             definition: @dictionary_word.definition,
                                             notes: @dictionary_word.notes)
    end

    it "word is not valid" do
      expect(@dictionary_word2).not_to be_valid
    end
  end

  context "when new dictionary word is composed of more than one word" do
    before(:example) do
      @dictionary_word2 = FactoryGirl.build(:two_words_dictionary_word)
    end

    it "word is not valid" do
      expect(@dictionary_word2).not_to be_valid
    end
  end
end
