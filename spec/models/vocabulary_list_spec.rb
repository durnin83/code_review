describe VocabularyList do
  before(:each) do
    @vocabulary_list = FactoryGirl.create(:vocabulary_list)
  end

  subject { @vocabulary_list }

  it { should respond_to(:name) }
  it { should respond_to(:group) }
  it { should respond_to(:description) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:group) }
  it { should validate_presence_of(:description) }

  context "when new vocabulary list contains valid vocabulary list words" do
    before(:example) do
      vocabulary_list_word_atts = FactoryGirl.attributes_for(:vocabulary_list_word)
      vocabulary_list_atts = FactoryGirl.attributes_for(:vocabulary_list)
      vocabulary_list_atts[:vocabulary_list_words_attributes] = [vocabulary_list_word_atts]
      @vacabulary_list_with_words = VocabularyList.new(vocabulary_list_atts)
    end

    it "vocabulary list is valid" do
      expect(@vacabulary_list_with_words).to be_valid
    end
  end

  context "when new vocabulary list contains words that are invalid" do
    before(:example) do
      vocabulary_list_word_atts = FactoryGirl.attributes_for(:two_words_vocabulary_list_word)
      vocabulary_list_atts = FactoryGirl.attributes_for(:vocabulary_list)
      vocabulary_list_atts['vocabulary_list_words_attributes'] = [vocabulary_list_word_atts]
      @vacabulary_list_with_words = VocabularyList.new(vocabulary_list_atts)
    end

    it "vocabulary list is not valid" do
      expect(@vacabulary_list_with_words).not_to be_valid
    end
  end

end
