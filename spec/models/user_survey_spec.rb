describe UserSurvey do
  before(:each) do
    @user = FactoryGirl.build(:user)
    @user_survey = FactoryGirl.create(:user_survey, user: @user)
  end

  subject { @user_survey }

  it { should respond_to(:user) }
  it { should respond_to(:code) }
  it { should respond_to(:answer) }

  it { should belong_to(:user) }
  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:answer) }
  it do
    should validate_inclusion_of(:code).in_array(UserSurvey::SURVEY_QUESTIONS.keys)
  end

  context "when new survey for a user has the same code as an exisiting survey for him" do
    before(:example) do
       @user_survey2 = FactoryGirl.create(:user_survey, user: @user )
    end

    it "new survey is not valid" do
      expect(@user_survey2).not_to be_valid
    end
  end

  context "when survey is a select field and value is not included in the list" do
    before(:example) do
      @user_survey.answer = 'Not in list'
    end

    it "is not valid" do
      expect(@user_survey).not_to be_valid
    end
  end
end