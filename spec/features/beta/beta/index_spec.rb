# Feature: Home page
#   As a visitor
#   I want to visit a beta page
#   So I can learn more about the website
feature 'Beta page' do

  # Scenario: Visit the beta page
  #   Given I am a visitor
  #   When I visit the beta page
  #   Then I see "Smart Gongbu" and "Never forget vocabulary words again" and a link to a video
  scenario 'visit the home page' do
    visit root_path
    expect(page).to have_content 'Smart Gongbu'
    expect(page).to have_content 'Never forget vocabulary words again'
    expect(page).to have_css("iframe[class='embed-responsive-item']")
  end

  # Scenario: Visit the beta page to view introductory video
  #   Given I am a visitor
  #   When I visit the beta page and click the video link
  #   Then I see a youtube video on a modal screen
  #   When I click the close button
  #   Then I don't see the video anymore
  scenario 'visit the home page and click video link', :js => true do
    visit root_path
    expect(page).to have_css("#video_link")
    page.first("#video_link").click
    expect(page).to have_css("iframe[src]")
    expect(page).to have_css("#video_modal button")
    page.first(:css, "#video_modal button").click
    expect(page).to have_no_css("iframe[src]")
  end

  # Scenario: Visit de the beta page and see navigation links
  #   Given I am a visitor
  #   When I visit the beta page
  #   Then I see "Support," "Contact," and "Sign up for Beta Test"
  scenario 'view navigation links' do
    visit root_path
    expect(page).to have_content 'SUPPORT'
    expect(page).to have_content 'CONTACT'
    expect(page).to have_content 'Login'
    expect(page).to have_content 'User Profile'
    expect(page).to have_content 'Sign up for Beta Test'
  end

end
