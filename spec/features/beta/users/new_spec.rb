# Feature: Sign up for beta
#   As a visitor
#   I want to sign up for beta
#   So I can recieve information and news of Smart Gongbu
feature 'Sign up for beta' do

  # Scenario: Visitor sees a page with all the sign up fields and a sign up confirmation button
  #   Given I am a visitor
  #   When I visit the sign up for beta page
  #   Then I see the following fields: First Name, Last Name, Email, Phone, Survey questions and confirmation button
  scenario 'visitor sees a page with all the sign up fields and a sign up confirmation button' do
    visit new_beta_user_path
    expect(page).to have_field('First name')
    expect(page).to have_field('Last name')
    expect(page).to have_field('Email')
    expect(page).to have_field('Phone')
    expect(page).to have_select('How did you find about Smart Gongbu?')
    expect(page).to have_select('How do you intend to use Smart Gongbu?')
    expect(page).to have_select('Is it ok if we contact you to give you more information about Smart Gongbu?')
    expect(page).to have_select('Is it ok if we contact you for feedback on Smart Gongbu?')
    expect(page).to have_button('Sign up for Beta Test')
  end

  # Scenario: Visitor can sign up with valid email and phone
  #   Given I haven't registered as beta user yet
  #   When I sign up with a valid email and phone
  #   Then I see thank you page
  scenario 'visitor can sign up with all valid fields' do
    sign_up_beta('Rodrigo', 'Martinez', 'durnin@hotmail.com', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    txts = ['Thank you for signing up','You will recieve an email from us when Smart Gongbu is ready to be beta tested', 'Back to Smart Gongbu']
    txts.each { |txt| expect(page).to have_content(txt) }
    expect(User.count).to be(1)
    expect((UserSurvey.joins(:user)).count).to be(4)
  end

  # Scenario: Visitor cannot sign up without any of the required fields: first name, last name, email, phone, about, intention, more_information, feedback
  #   Given I haven't registered as beta user yet
  #   When I sign up without any attributes set
  #   Then I see an error indicating that each attribute is required
  scenario 'visitor cannot sign up without any of the required fields: first name, last name, email, phone, about, intention, more_information, feedback' do
    sign_up_beta('', '', '', '', '', '', '', '')
    expected = "can't be blank"
    expect(page.find(:xpath, "//*[@id='user_first_name']/../..")['title']).to eq(expected)
    expect(page.find(:xpath, "//*[@id='user_last_name']/../..")['title']).to eq(expected)
    expect(page.find(:xpath, "//*[@id='user_email']/../..")['title']).to eq(expected)
    expected = "is invalid, can't be blank"
    expect(page.find(:xpath, "//*[@id='user_phone']/../..")['title']).to eq(expected)
    expected = "can't be blank, is not included in the list"
    expect(page.find(:xpath, "//*[@id='user_user_surveys_attributes_0_answer']/../..")['title']).to eq(expected)
    expect(page.find(:xpath, "//*[@id='user_user_surveys_attributes_1_answer']/../..")['title']).to eq(expected)
    expect(page.find(:xpath, "//*[@id='user_user_surveys_attributes_2_answer']/../..")['title']).to eq(expected)
    expect(page.find(:xpath, "//*[@id='user_user_surveys_attributes_3_answer']/../..")['title']).to eq(expected)
  end

  # Scenario: Visitor cannot sign up with invalid email
  #   Given I haven't registered as beta user yet
  #   When I sign up with an invalid email
  #   Then I see an error indicating that email is invalid
  scenario 'visitor cannot sign up with invalid email' do
    sign_up_beta('Rodrigo', 'Martinez', 'pepito@cosito', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "is invalid"
    expect(page.find(:xpath, "//*[@id='user_email']/../..")['title']).to eq(expected)
  end


  # Scenario: Visitor cannot sign up with invalid phone
  #   Given I haven't registered as beta user yet
  #   When I sign up with an invalid phone
  #   Then I see an error indicating that phone is invalid
  scenario 'visitor cannot sign up with invalid phone' do
    sign_up_beta('Rodrigo', 'Martinez', 'durnin@hotmail.com', '1234', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "is invalid"
    expect(page.find(:xpath, "//*[@id='user_phone']/../..")['title']).to eq(expected)
  end

  # Scenario: Visitor cannot sign up with twice with the same email
  #   Given I have previously registered with a certain email as beta user
  #   When I sign up with the same email
  #   Then I see an error indicating that my email is already registered
  scenario 'visitor cannot sign up with the same email twice' do
    sign_up_beta('Rodrigo', 'Martinez', 'durnin@hotmail.com', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    sign_up_beta('Pablo', 'Martinez', 'durnin@hotmail.com', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "has already been taken"
    expect(page.find(:xpath, "//*[@id='user_email']/../..")['title']).to eq(expected)
  end

end