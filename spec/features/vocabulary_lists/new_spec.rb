# Feature: Create a Vocabulary List
#   As a visitor
#   I want to create a vocabulary list
feature 'Create Vocabulary List' do

  # Scenario: Visitor sees a page with all the vocabulary list fields and save button
  #   Given I am a visitor
  #   When I visit the create vocabulary list page
  #   Then I see the following fields: Name, Group, Description, Word List file and Save button
  scenario 'visitor sees a page with all the vocabulary list fields and save button' do
    visit new_vocabulary_list_path
    expect(page).to have_field('Name')
    expect(page).to have_field('Group')
    expect(page).to have_field('Description')
    expect(page).to have_field('Word List File')
    expect(page).to have_button('Save')
  end

  # Scenario: Visitor creates a vocabulary list without words
  #   Given I am a visitor
  #   When I visit the create vocabulary list page, enter the Name, Group and Description of VL
  #   Then I see index page with the new vocabulary list
  scenario 'visitor creates a vocabulary list without words' do
    fill_in 'Name', with: 'Some name'
    fill_in 'Group', with: 'Some group'
    fill_in 'Description', with: 'Some description'
    click_button 'Save'
    txts = ['Thank you for signing up','You will recieve an email from us when Smart Gongbu is ready to be beta tested', 'Back to Smart Gongbu']
    txts.each { |txt| expect(page).to have_content(txt) }
    expect(VocabularyList.count).to be(1)
    expect((VocabularyList.joins(:vocabulary_list_words)).count).to be(0)
  end

  # Scenario: Visitor creates a vocabulary list with words
  #   Given I am a visitor
  #   When I visit the create vocabulary list page, enter the Name, Group, Description and Word List File of VL
  #   Then I see index page with the new vocabulary list
  scenario 'visitor cannot sign up without any of the required fields: first name, last name, email, phone, about, intention, more_information, feedback' do
    fill_in 'Name', with: 'Some name'
    fill_in 'Group', with: 'Some group'
    fill_in 'Description', with: 'Some description'
    click_button 'Save'
    txts = ['Thank you for signing up','You will recieve an email from us when Smart Gongbu is ready to be beta tested', 'Back to Smart Gongbu']
    txts.each { |txt| expect(page).to have_content(txt) }
    expect(VocabularyList.count).to be(1)
    expect((VocabularyList.joins(:vocabulary_list_words)).count).to be(0)
  end

  # Scenario: Visitor cannot sign up with invalid email
  #   Given I haven't registered as beta user yet
  #   When I sign up with an invalid email
  #   Then I see an error indicating that email is invalid
  scenario 'visitor cannot sign up with invalid email' do
    sign_up_beta('Rodrigo', 'Martinez', 'pepito@cosito', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "is invalid"
    expect(page.find(:xpath, "//*[@id='user_email']/../..")['title']).to eq(expected)
  end


  # Scenario: Visitor cannot sign up with invalid phone
  #   Given I haven't registered as beta user yet
  #   When I sign up with an invalid phone
  #   Then I see an error indicating that phone is invalid
  scenario 'visitor cannot sign up with invalid phone' do
    sign_up_beta('Rodrigo', 'Martinez', 'durnin@hotmail.com', '1234', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "is invalid"
    expect(page.find(:xpath, "//*[@id='user_phone']/../..")['title']).to eq(expected)
  end

  # Scenario: Visitor cannot sign up with twice with the same email
  #   Given I have previously registered with a certain email as beta user
  #   When I sign up with the same email
  #   Then I see an error indicating that my email is already registered
  scenario 'visitor cannot sign up with the same email twice' do
    sign_up_beta('Rodrigo', 'Martinez', 'durnin@hotmail.com', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    sign_up_beta('Pablo', 'Martinez', 'durnin@hotmail.com', '10 1234 5678', 'Referral by a friend', 'To learn', 'Yes', 'Yes')
    expected = "has already been taken"
    expect(page.find(:xpath, "//*[@id='user_email']/../..")['title']).to eq(expected)
  end

end