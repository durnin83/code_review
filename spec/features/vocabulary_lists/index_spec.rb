# Feature: Vocabulary Lists Index
#   As a user
#   I want to see vocabulary lists
feature 'Vocabulary Lists Index' do

  # Scenario: Visit the vocabulary lists page
  #   Given I am a user
  #   When I visit the vocabulary lists index page
  #   Then I see "Vocabulary Lists" title, the table headers (attributes of list) and the create new button
  scenario 'visit the home page' do
    visit vocabulary_lists_path
    expect(page).to have_content 'Vocabulary Lists'
    expect(page).to have_content 'Name'
    expect(page).to have_content 'Group'
    expect(page).to have_content 'Description'
    expect(page).to have_content 'Updated'
    expect(page).to have_content 'Create New'
  end

end