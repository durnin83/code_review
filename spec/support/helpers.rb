require 'support/helpers/session_helpers'
require 'support/helpers/beta_helpers'
RSpec.configure do |config|
  config.include Features::SessionHelpers, type: :feature
  config.include Features::BetaHelpers, type: :feature
end
