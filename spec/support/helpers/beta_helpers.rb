module Features
  module BetaHelpers
    def sign_up_beta(first_name, last_name, email, phone, about, intention, more_information, feedback)
      visit new_beta_user_path
      fill_in 'First name', with: first_name
      fill_in 'Last name', with: last_name
      fill_in 'Email', with: email
      fill_in 'Phone', with: phone
      select about, from: 'How did you find about Smart Gongbu?', :match => :first
      select intention, from: 'How do you intend to use Smart Gongbu?', :match => :first
      select more_information, from: 'Is it ok if we contact you to give you more information about Smart Gongbu?', :match => :first
      select feedback, from: 'Is it ok if we contact you for feedback on Smart Gongbu?', :match => :first
      click_button 'Sign up for Beta Test'
    end
  end
end
