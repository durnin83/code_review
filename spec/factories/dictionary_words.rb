FactoryGirl.define do
  factory :dictionary_word do
    word Faker::Lorem.word
    part_of_speech DictionaryWord::PART_OF_SPEECH_VALUES[0]
    translation Faker::Lorem.word
    definition Faker::Lorem.sentence
    notes Faker::Lorem.paragraph

    factory :two_words_dictionary_word do
      word Faker::Lorem.words(2)
    end
  end
end
