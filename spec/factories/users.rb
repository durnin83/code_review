FactoryGirl.define do
  factory :user do
    name Faker::Internet.user_name
    email Faker::Internet.email
    #password Faker::Internet.password(8)
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    phone '10 1234 5678'

    trait :admin do
      role 'admin'
    end

    trait :beta do
      role 'beta'
    end

  end
end
