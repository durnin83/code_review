FactoryGirl.define do
  factory :vocabulary_list do
    name Faker::Lorem.sentence
    group Faker::Lorem.sentence
    description Faker::Lorem.paragraph

    factory :vocabulary_list_with_words do
      transient do
        words_count 5
      end

      after(:build) do |vocabulary_list, evaluator|
        build_list(:vocabulary_list_word, evaluator.words_count, vocabulary_list: vocabulary_list)
      end
    end

    factory :vocabulary_list_with_invalid_words do
      transient do
        words_count 5
      end

      after(:build) do |vocabulary_list, evaluator|
        build_list(:two_words_vocabulary_list_word, evaluator.words_count, vocabulary_list: vocabulary_list)
      end
    end
  end
end
