FactoryGirl.define do
  factory :user_survey do
    user
    code UserSurvey::SURVEY_QUESTIONS.keys.first
    answer UserSurvey::SURVEY_QUESTIONS_OPTIONS[UserSurvey::SURVEY_QUESTIONS.keys.first][0]
  end

end
