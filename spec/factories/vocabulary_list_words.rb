FactoryGirl.define do
  factory :vocabulary_list_word do
    vocabulary_list
    word Faker::Lorem.word
    part_of_speech DictionaryWord::PART_OF_SPEECH_VALUES[0]
    translation Faker::Lorem.word
    definition Faker::Lorem.sentence
    notes Faker::Lorem.paragraph

    factory :two_words_vocabulary_list_word do
      word Faker::Lorem.words(2).join(' ')
    end
  end

end
